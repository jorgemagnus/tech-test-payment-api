using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
    Uma venda contém informação sobre o vendedor que a efetivou, data, 
    identificador do pedido e os itens que foram vendidos;
    O vendedor deve possuir id, cpf, nome, e-mail e telefone;
*/

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string ItemDescricaoProdutoVenda { get; set; }
        public DateTime DataVenda { get; set; }
        public int CodigoVendedor { get; set; } //o Id do vendedor.
        public string NomeVendedor { get; set; }
        public string CpfVendedor { get; set; }
        public string TelefoneVendedor { get; set; }
        public string EmailVendedor { get; set; }
        public EnumStatusVenda Status { get; set; }

    }
}