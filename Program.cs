using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<VendaContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));
/*
    para que o string de conexão funcionar corretamento no meu pc, foi necessário adicionar 
    o parâmetro "Trust Server Certificate=True" no final da respectiva string.
    ver em => {} appsettings.Development.json
*/
builder.Services.AddControllers().AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
/*
    para que o enum na classe EnumStatusVenda.cs retorna-se os devidos valores corretamente no Swagger,
    foi necessário informar os comandos acima, dessa forma ao invés de vim 0,1,2,3 e 4, veio as opções
    AguardandoPagamento, PagamentoAprovado,  EnviadoParaTransportadora, Entregue e  Cancelada.
*/

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
