using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;


        public VendaController(VendaContext context)
        {
            _context = context;
        }
       
       
        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodasVendas()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }


        [HttpGet("ObterVendaPorStatus")]
        public IActionResult ObterVendaPorStatus(EnumStatusVenda status)
        {                     
            var venda = _context.Vendas.Where(x => x.Status == status);        
            return Ok(venda);
        }

      
        [HttpGet("LocalizarVenda{id}")]
        public IActionResult LocalizarVenda(int id)
        {
            var venda = _context.Vendas.Find(id);

            //Obrigatório id da venda.
            if (venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }


        [HttpGet("LocalizarPorNomeDoVendedor{nomeVendedor}")]
        public IActionResult LocalizarPorNomeDoVendedor(string nomeVendedor) 
        {            
            var vendaBanco = _context.Vendas.Where(x => x.NomeVendedor.Contains(nomeVendedor));            
            return Ok(vendaBanco);
        }


        [HttpPost("GravarVenda")]
        public IActionResult GravarVenda(Venda venda)
        {
            //obrigatório
            if (venda.ItemDescricaoProdutoVenda == null)
            {
                return BadRequest(new { Erro = "Obrigatório a venda ter pelo menos 1 item" });
            }
            
            if (venda.DataVenda == DateTime.MinValue)
            {
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });
            }  

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(LocalizarVenda), new { id = venda.Id }, venda);
        }


        [HttpPut("AprovarPagamentoVenda{id}")]
        public IActionResult AprovarPagamentoVenda(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }

            //De: Aguardando pagamento  Para: Pagamento Aprovado   
            

            if (vendaBanco.Status == EnumStatusVenda.AguardandoPagamento)
            {
                vendaBanco.Status = EnumStatusVenda.PagamentoAprovado;
            }else
            {
               return BadRequest(new {Erro = "Obrigatório que a venda esteja em situação de Aguardando Pagamento!." +"Status atual:"+ vendaBanco.Status});  
            } 

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }


        [HttpPut("EnviarVendaParaTransportadora{id}")]
        public IActionResult EnviarVendaParaTransportadora(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }

            //De: Pagamento Aprovado    Para: Enviado para Transportadora            
            

            if (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado)
            {
                vendaBanco.Status = EnumStatusVenda.EnviadoParaTransportadora;
            }
            else
            {
               return BadRequest(new {Erro = "Obrigatório que a venda esteja com Pagamento Aprovado!." +"Status atual:"+ vendaBanco.Status});  
            } 

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

       
        [HttpPut("VendaEntregue{id}")]
        public IActionResult VendaEntregue(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }                

            //De: Enviado para Transportador. Para: Entregue            

            if (vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora) 
            {
                vendaBanco.Status = EnumStatusVenda.Entregue; 
            }
            else
            {
               return BadRequest(new {Erro = "Obrigatório que o produto esteja na transportadora!." +"Status atual:"+ vendaBanco.Status});  
            }                                      
           
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }


        [HttpPut("CancelarVenda{id}")]
        public IActionResult CancelarVenda(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            /*Cancelar venda somente se:
              De: Aguardando pagamento   Para: Cancelada 
              De: Pagamento Aprovado     Para: Cancelada 
            */

            if((vendaBanco.Status == EnumStatusVenda.AguardandoPagamento) || (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado))
            {
                vendaBanco.Status = EnumStatusVenda.Cancelada;
            }
            else
            {
               return BadRequest(new {Erro = "Para cancelar a venda é necessário que o status esteja Aguardando Pagamento ou Pagamento Aprovado!."+"Status atual:"+ vendaBanco.Status});  
            }                         

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }
       

    }   
}